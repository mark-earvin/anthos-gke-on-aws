gcp_project_id          = "xxx"
admin_user              = "xxx"
name_prefix             = "aws-cluster"
# node_pool_instance_type = "t3.medium" # demo
node_pool_instance_type = "m5.large" # prod/preprod
cluster_version         = "1.21.6-gke.1500" # "1.21.5-gke.2800"
#--Use 'gcloud container aws get-server-config --location [gcp-region]' to see Availability --
gcp_location              = "australia-southeast1"
aws_region                = "ap-southeast-2"
subnet_availability_zones = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
asg_min_nodes        = 2
asg_max_nodes        = 5


# Possible extensions
# 1. Proper Naming Conventions
# 2. ASM and ACM intergation
# 3. (Not recommended but possible) Use of pre-existing AWS VPC architecture.
